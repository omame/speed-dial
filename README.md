# Speed Dial

## Why do you need this?

I recently got fed up with the memory hog that Google Chrome has become
(sorry folks, no hard feelings) and I've been an avid user of Speed Dial 2 for a
long time. Sadly, Safari isn't supported so I decided to invest a couple of
hours to create a static page that does what I need. Since it's a generic HTML
page you can use it in any browser that supports a local homepage, which I
believe are the vast majority out there.

## Understandable. So how do I use this?

Clone the repo and make a copy of `sites.js.example` called `sites.js`. Then
populate the JSON file with your bookmarks. The keys on the first level are the
group names. They will appear on the top of the page and will change the
bookmarks when you hover with the mouse pointer. Then inside the groups you can
add your bookmarks assigning the bookmark name, the bookmark uri and an icon
uri.

Once your sites are ready you can configure your browser to load the local html.
For Safari, open the preferences and select the "General" tab. Set "New windows
open with" and "New tabs open with" to homepage and insert the full `file:///`
path to the repo. You're done.

## It works, but it's got a very rough UX...

I'm the opposite of a front-end designer so this is already more than I thought
I could achieve. I'd be thrilled if you can contribute and make this fancier
while maintaining its simplicity.

## Known issues

- When opening a new tab the search bar doesn't have the focus. - #1
