function showgroups() {
	var groups = document.getElementById('groups');
	var list = document.createElement('ul');
	list.setAttribute('class', 'groups');
	groups.appendChild(list);
	for (group_name of Object.keys(sites)) {
		group = document.createElement('li');
		group.setAttribute('onmouseover','showbookmarks("'+group_name+'")');
		// group.setAttribute('onmouseout','');
		group.innerHTML = group_name;
		list.appendChild(group);
	}
}

function showbookmarks(group_name) {
	content = document.getElementById('content');
	// add <ul id="Group" class="favourites">
	list = document.createElement('ul');
	list.setAttribute('id', group_name);
	list.setAttribute('class', 'favourites');
	if (content.hasChildNodes()) {
		content.replaceChild(list, content.firstChild);
	} else {
		content.appendChild(list);
	}
	var group = sites[group_name];
	// now add the icons
	for (var i in group) {
		// add <li>
		var item = document.createElement('li');

		// add <a href="i.url">
		var link = document.createElement('a');
		link.setAttribute('href', group[i].url);

		// add <div>
		var block = document.createElement('div');

		// add <img src="lower_strip(i.name).png" alt="i.name">
		var icon = document.createElement('img');
		icon.setAttribute('src', group[i].icon);
		icon.setAttribute('alt', group[i].name);

		// add <p>Title</p>
		var title = document.createElement('p')
		title.innerHTML = group[i].name;

		// put everything together
		list.appendChild(item);
		item.appendChild(link);
		link.appendChild(block);
		block.appendChild(icon);
		link.appendChild(title);
		if (i % 3 === 2) {
			list.appendChild(document.createElement('br'));
		}
	}
}

showgroups();
showbookmarks(Object.keys(sites)[0]);
